<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('kontakt', 'ContactController@index');
Route::post('kontakt/dodaj', 'ContactController@add')->name('kontakt.dodaj');
Route::get('kontakt/poruka-je-poslata', 'ContactController@success');
Route::get('kontakt/aktivacija-naloga/{email_code}', 'Auth\RegisterController@activation');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('proizvodi', 'ItemController@index');
Route::get('proizvod/{id}/{title}', 'ItemController@details')->where('id', '[0-9]+');
Route::get('kategorija/{catId}/{catName}', 'ItemController@category')->where('catId', '[0-9]+')->name('kategorija');
Route::get('dodaj-u-korpu/{id}/{title}', 'ItemController@addToCart')->where('id', '[0-9]+');
Route::get('korpa', 'ItemController@cart');
Route::get('izbaci-iz-korpe/{num}', 'ItemController@removeFromCart')->where('num', '[0-9]+');
Route::get('naruci', 'ItemController@order');

Auth::routes();
