@extends('public/layout')


@section('content')

            <h1>Login</h1>
            <div class="row">
                <div class="col-sm-4">
                    <form action="{{ url('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email" >
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password" name="password" >
                        </div>
                        <button type="submit" class="btn btn-primary">login</button>
                    </form>
                </div>
            </div>
            <p class="registration">Ukoliko nemate kreiran nalog, morate da se <a href="{{ url('register') }}">registrujete</a>.</p>
            
@endsection        