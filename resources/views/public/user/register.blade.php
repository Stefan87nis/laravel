@extends('public/layout')

@section('content')
    <h1>Registracija</h1>
    <div class="row">
        <div class="col-sm-4">
            <form action="{{ url('/register') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" >
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" >
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password-confirm">Confirm Password:</label>
                    <input type="password" class="form-control" id="password-confirm"   name="password_confirmation">
                </div>
                <div class="form-group">
                    <label for="first_name">Ime:</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}" >
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="last_name">Prezime:</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" >
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="address">Adresa:</label>
                    <input type="text" class="form-control" id="address" name="address"  value="{{ old('address') }}">
                    @if ($errors->has('address'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="phone">Telefon:</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" >
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">registruj se</button>
            </form>
        </div>
    </div>
@endsection