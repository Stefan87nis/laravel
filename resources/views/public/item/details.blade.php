@extends('public/layout')

@section('content')

<h1>Proizvod</h1>
            
<h3>{{ $item->title }}</h3>

<div class="row">
    <div class="col-sm-5 col-md-5">
        <div class="">
            <img class="img-responsive" src="{{ asset('storage/proizvodi/' . $item->id . '/300x300_' . $item->image) }}" alt="{{ $item->title }}">
        </div>
    </div>
    <div class="col-sm-7 col-md-7">
        <p>
            {!! $item->description !!}
        </p>
        <p class="price">{{ $item->price }} RSD</p>
        @if(Auth::check())
             <p><a href="{{ url('dodaj-u-korpu/' . $item->id . '/' . str_slug($item->title) ) }}" class="btn btn-primary" role="button">dodaj u korpu</a></p>
         @endif
         
         @if (session('success'))
             <div class="alert alert-success col-sm-4">
                 {{ session('success') }}
             </div>
         @endif
    </div>
</div>

@endsection