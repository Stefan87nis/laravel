@extends('public/layout')

@section('content')


            <h1>Proizvodi</h1>
            <div class="item_search row">
                <form method="get" action="" class="col-sm-4 form-inline">
                    <div class="form-group">
                        <label for="pretraga">Pretraga:</label>
                        <input type="text" class="form-control" id="pretraga" name="pretraga" value="{{ $pretraga }}">
                        <input class="btn btn-primary" type="submit" value="ok">
                    </div>
                </form>
            </div>
            
            <div class="row items_list">
               @foreach ($items as $item) 
                <div class="col-sm-4 col-md-3">
                  <div class="thumbnail text-center">
                    <div class="item_image">
                        <img class="img-responsive" src="{{ asset('storage/proizvodi/' . $item->id . '/160x160_' . $item->image) }}" alt="{{ $item->title }}">
                    </div>
                    <div class="caption">
                      <h3>"{{ $item->title }}"</h3>
                      <p class="price">{{ $item->price }} RSD</p>
                      <p><a href="{{ url('proizvod/' .  $item->id . '/' . str_slug($item->title) ) }}" class="btn btn-primary" role="button">detaljnije</a></p>
                    </div>
                  </div>
                </div>
                @endforeach
            </div>
    {{ $items->links() }}
@endsection    