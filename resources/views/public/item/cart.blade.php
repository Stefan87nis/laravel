@extends('public/layout')

@section('content')

<h1>Korpa</h1>
            
@if (session('cart'))
    <h3>Proizvodi u korpi</h3>

    <div class="table-responsive">

        <table class="table table-striped table-bordered table-condensed">
            <tr class="info">
                <td>Slika</td>
                <td>Proizvod</td>
                <td>Kategorija</td>
                <td>Cena</td>
                <td>&nbsp;</td>
            </tr>
            @php
                $totalPrice = 0;
            @endphp

            @foreach( session('cart', array()) as $num => $item )
                <tr>
                    <td><img width="50" src="{{ asset('storage/proizvodi/' . $item->id . '/160x160_' . $item->image) }}" alt="{{ $item->title }}"></td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->category->name }}</td>
                    <td>{{ $item->price }}</td>
                    <td><a href="{{ url('izbaci-iz-korpe/' . $num) }}" style="color:red; "><b>izbaci proizvod</b></a></td>
                </tr>
                @php
                    $totalPrice += $item->getAttributes()['price'];
                @endphp
            @endforeach
        </table>
    </div>

    <p class="br_proivoda">Broj proizvoda u korpi: <b>{{ count(session('cart')) }}</b></p>
    <p class="br_proivoda">Ukupna cena: <b style="color:red">{{ number_format($totalPrice, 2, ',', '.') }} RSD</b></p>

    <div>
        <a href="{{ url('naruci') }}" class="btn btn-primary">Naruči</a>
    </div>

@else 

    <h3>Vasa korpa je prazna!</h3>

@endif

@endsection