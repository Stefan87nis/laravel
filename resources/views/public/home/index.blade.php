@extends('public/layout')

@section('content')
            <h1>{{ $homePage->title }}</h1>
            <div class="row">
                <div class="col-sm-4"><img src="{{ asset('images/onlineshop.png') }}" class="img-responsive"></div>
                <div class="col-sm-8">
                    {!! $homePage->description !!}
                </div>
            </div>

@endsection