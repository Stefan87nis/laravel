<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigacija" aria-expanded="false">
                      <span class="sr-only"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navigacija">
                    <ul class="nav navbar-nav">
                      <li class="active"><a href="{{ url('/') }}">Home</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Proizvodi <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="{{ url('Proizvodi') }}">Sve kategorije</a></li>
                          <li role="separator" class="divider"></li>
                    @foreach($navCategories as $navCategory)
                           <li>
                                <a href="{{ route('kategorija', [$navCategory->id, str_slug($navCategory->name)] ) }}">
                        {{$navCategory->name}} ({{$navCategory->num_of_items}})
                               </a>
                           </li>
                   @endforeach
                        </ul>
                      </li>
                      <li><a href="{{ url('kontakt') }}">Kontakt</a></li>
                    @if ( Auth::check() )
                        <li><a href="{{ url('logout') }}">Logout</a></li>
                    @else
                        <li {!! url()->current() == url('login') ? 'class="active"' : '' !!} ><a href="{{ url('login') }}">Login</a></li>
                    @endif
                    </ul>
                </div>
            </div>
</nav>