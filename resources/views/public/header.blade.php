<div class="header clearfix">
            <div class="logo">Online Shop</div>
            @if ( Auth::check() )
    	<div class="header_login">
            <b>{{ Auth::user()->email }}</b> <br>
            ({{ Auth::user()->name }})<br>
            <a href="{{ url('korpa') }}">korpa</a>: {{ count(session('cart') ) }} proizvoda <br>
            @php
            	$totalPrice = 0;
            @endphp

            @foreach(session('cart', array() ) as $cartItem)
            	@php
                    $totalPrice += $cartItem->getAttributes()['price'];
                @endphp
            @endforeach
            <span class="price">{{ number_format($totalPrice, 2, ',', '.') }} RSD</span>
       </div>
    @endif
        </div>