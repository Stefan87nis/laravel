@extends('public/layout')

@section('content')
            <h1>{{ $contactPage->title }}</h1>
            <div class="row">
                <div class="col-sm-6">
                    {!! $contactPage->description !!}
                    
                    <h2>Kontaktirejte nas!</h2>
                    @if ($errors->any())
                                <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                        </div>
                    @endif
                    
                    <form action="{{ route('kontakt.dodaj') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Ime i prezime:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label for="text">Tekst:</label>
                            <textarea  class="form-control" id="text" name="text" rows="4">{{ old('text') }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">pošalji</button>
                    </form>
                </div>
                <div class="col-sm-6">
                    <iframe style="width: 100%" width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.rs/maps?q=43.317821,21.902618&amp;num=1&amp;ie=UTF8&amp;t=m&amp;z=15&amp;ll=43.317794,21.902924&amp;output=embed"></iframe>
                </div>
            </div>
  </body>
@endsection