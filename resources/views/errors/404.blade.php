@extends('public/layout')

@section('content')
    <h1>Greška</h1>
    <p class="alert alert-danger">
        Stranica ne postoji!
    </p>
@endsection