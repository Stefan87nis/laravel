<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Item extends Model
{
    
    function category(){
        
        return $this->belongsTo('App\Category');
    }
    public function getPriceAttribute($value)
 	   {
        return number_format($value, 2, ',', '.');
    }
    public function purchases()
    {
        return $this->belongsToMany('App\Purchase');
    }
}
