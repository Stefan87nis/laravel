<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Purchase;
use Auth;


class ItemController extends Controller
{
    
    
    
    public function index(Request $request){
        
        
        $pretraga = $request->has('pretraga') ? $request->pretraga : '';
        
        $items = Item::where('title', 'LIKE', "%$pretraga%")
                    ->orWhere('description', 'LIKE', "%$pretraga%")
                    ->paginate(8, ['*'], 'strana');
        
        return view('public/item/index')->with('items', $items)
                ->with('items', $items->appends( ['pretraga' => $pretraga] ) )
                ->with('pretraga', $pretraga);
    }
    
    
    public function category(Request $request, $catId, $catName)
    {
    	$pretraga = $request->has('pretraga') ? $request->pretraga : '';

    	$items = Item::where('category_id', $catId)
                    ->whereRaw("(`title` LIKE '%$pretraga%' OR `description` LIKE '%$pretraga%')")
                    ->paginate(8, ['*'], 'strana');

    	return view('public/item/index')
    			->with('items', $items->appends( ['pretraga' => $pretraga] ) )
    			->with('pretraga', $pretraga);
    }
    
    
    public function addToCart($id, $title) 
    {
	$item = Item::find($id);

        $cart = session('cart', array());

        $cart[] = $item;
        session(['cart' => $cart]);

        return back()->with('success', 'Dodali ste proizvod u korpu!');
    }
    
    public function cart() 
    {
    	return view('public/item/cart');
    }
    
    public function removeFromCart($num) 
    {
        $cart = session('cart', array());

        unset($cart[$num]);
        session(['cart' => $cart]);

        return back();
    }
    
    public function order() 
    {
        $userId = Auth::user()->id;

        $items = session('cart', array());

        $totalPrice = 0;
        foreach ($items as $item) {
            $totalPrice += $item->getAttributes()['price'];
        }

        $amount = count($items);

        $purchase = new Purchase([
            'user_id' => $userId,
            'amount' => $amount,
            'total_price' => $totalPrice
        ]);
        $purchase->save();
        
        foreach($items as $item) {
            $purchase->items()->attach($item->id);
        }
        
        session( ['cart' => array() ]);

        return redirect('/');
    }
    
    
    
    public function details($id, $title) 
     {
         $item = Item::find($id);
         
         return view('public/item/details')->with('item', $item);
     }
}
