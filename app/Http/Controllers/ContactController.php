<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Contact;

class ContactController extends Controller
{
    public function index(){
        
        
        $contactPage = page::find(2);
        
        return view('public/contact/index')->with('contactPage', $contactPage);
    }
    public function add(Request $request){
        
        $this->validate($request,
                [
                  'name'=>'required',
                  'email'=>'required|email',
                  'text'=>'required'
                ],
                [
                  'required'=>'Niste popunili<b>attribute</b>',
                  'email'=>'Upisite ispravnu<b>email adresu</b>'
                ]
                );
                
                $contact = new Contact();
                
                $contact->name = $request->name;
                $contact->email = $request->email;
                $contact->text = $request->text;
                
                $contact->save();
                
                return redirect('kontakt/poruka-je-poslata');
    }
    public function success()
            {
                    return view('public/contact/success');
            }

}
