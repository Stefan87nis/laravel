<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Page;
class HomeController extends Controller
{
    public function index(){

        $homePage =  Page::find(1);
        
        return view('public/home/index')->with('homePage' , $homePage);
    }
}
