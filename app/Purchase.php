<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = [
        'user_id', 
        'amount', 
        'total_price', 
        'purchase_id', 
        'item_id'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}