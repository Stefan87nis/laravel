<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
   public $timestamps = false;
   
   protected $fillable = ['name'];
   
   function items(){
       
   return $this->hasMany('App\Item');
}
}
