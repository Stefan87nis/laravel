<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert
                ([
                    'title'=>'HOME',
                    'description'=>
                    '<p style="margin: 15px;">Dobro do&scaron;li na <strong>Online Shop</strong>!</p>
                    <p style="margin: 15px;"><span style="color: #0000ff;">Uz na&scaron; efikasan sistem prikaza i pretrage, </span><br /><span style="color: #0000ff;"> brzo i lako ćete doći do željenog proizvoda iz na&scaron;e bogate kolekcije.</span></p>
                    <p style="margin: 15px;"><span style="background-color: #ffffff;"><em>Potrebno je svega nekoliko koraka od <a href="registracija.html" style="text-decoration: underline;">registracije</a> </em></span><br /><span style="background-color: #ffffff;"><em> do dostave paketa na Va&scaron;u adresu.</em></span></p>
                    <p style="margin: 15px;"><span style="background-color: #00ff00;">Želimo Vam ugodnu kupovinu.</span></p> '
                ]);
        DB::table('pages')->insert
                ([
                    'title'=>'KONTAKT',
                    'description'=>
                    '<p> 
                        Tel: 018/111-555 <br /> 
                        Mob: 069/123-4567 <br /> 
                        Mob: 069/123-2222<br /> 
                        Email: <a href="mailto:office@onlineshop.com">office@onlineshop.com</a>
                    </p>'

                ]);
    }
}
