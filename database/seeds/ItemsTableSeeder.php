<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dvd = Category::find(1);
        $dvd->items()->saveMany([
            new Item(['title' => 'Gospodar prstenova LOTR',
                    'description' => 'Original:LORD OF THE RINGS: FELLOWSHIP OF THE RING Žanr: Fantazija Trajanje: 208 minuta Režija: Piter Džekson Scenario: Dž. R. R. Tolkin, Piter Džekson, Fran Volš, Filipa Bojens Uloge: Elajdža Vud, Ijan Makelen, Vigo Mortensen, Šon Astin, Kejt Blašet, Liv Tajler, Džon Ris-Dejvis, Dominik Monahan, Bili Bojd, Orlando Blum, Endi Serkis, Kristofer Li, Hjugo Viving',
                    'price' => 2500.00,
                    'image' => 'gospodar_prstenova.jpg']),
            new Item(['title' => 'TAXI 4',
                    'description' => 'Žanrovi: Avanturistički, Akcioni Glumci: Samy Naceri',
                    'price' => 300.00,
                    'image' => 'taxi4.jpg']),
            new Item(['title' => 'Lara Croft: Tomb Raider',
                    'description' => 'Video game adventurer Lara Croft comes to life in a movie where she races against time and villains to recover powerful ancient artifacts.',
                    'price' => 750.00,
                    'image' => 'Tomb Raider.jpg']),
            new Item(['title' => 'Svemirski Vojnici',
                    'description' => 'Žanrovi: Naučna fantastika, Nagrađene knjige Izdavač: Čarobna knjiga',
                    'price' => 788.40,
                    'image' => 'svemirski_vojnici.jpg'])
        ]);
        $dvd->num_of_items = 4;
        $dvd->save();
        
        $knjige = Category::find(2);
        $knjige->items()->saveMany([
            new Item(['title' => 'Ana Karenjina',
                    'description' => 'Pisac: Lav Nikolajevič Tolstoj Žanrovi: Ljubavni, Klasična književnost',
                    'price' => 1300.00,
                    'image' => 'ana_karenjina.jpg']),
            new Item(['title' => 'Svemirski vojnici',
                    'description' => 'Svemirski vojnici (engl. Starship Troopers) je američki naučnofantastični film iz 1997. zasnovan na istoimenoj knjizi Roberta A. Hajnlajna (Robert A. Heinlein). Režirao ga je Pol Verhoven (Paul Verhoeven)',
                    'price' => 550.00,
                    'image' => 'svemirski_vojnici_knjiga.jpg']),
            new Item(['title' => 'Bjuik 8',
                    'description' => 'Pisac: Stiven King Žanrovi: naučna fantastika, horor',
                    'price' => 850.00,
                    'image' => 'bjuik 8.jpg'])
        ]);
        $knjige->num_of_items = 3;
        $knjige->save();
        
        $racunari = Category::find(3);
        $racunari->items()->saveMany([
            new Item(['title' => 'Laptop DELL Inspiron 3520 red 15.6',
                    'description' => 'Dell Inspiron 3520 je namenjen onima, koji od računara zahtevaju mobilnost i pouzdanost. Ovaj laptop poseduje Intel Celeron DualCore B820 procesor, Intel HD 3000 grafiku i 2GB RAM memorije.',
                    'price' => 31990.00,
                    'image' => 'Laptop DELL Inspiron 3520.jpg']),
            new Item(['title' => 'Računar Altos SUPRIMO, Z77/i7-3770K/16GB/GTX670/SSD+2TB/DVD',
                    'description' => 'Izuzetan i7 procesor u kombinaciji sa perfektnom MSI pločom čine sklad koji do sada nije vidjen kod personalnih računara. Nevorovatnih 16GB radne memorije.',
                    'price' => 137999.00,
                    'image' => 'Altos-Suprimo-Storia.jpg'])
        ]);
        $racunari->num_of_items = 2;
        $racunari->save();
    }
}
